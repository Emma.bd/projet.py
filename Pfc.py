def PFC():
    """
    Permet de jouer à "Pierre, Feuille, Ciseaux"
    """
    mains = {"P" : 1, "F" : 2, "C" : 3}
    
    # annonce qu'un chiffre va correspondre à la Pierre, la Feuille et aux Ciseaux
    
    print ("Jouons à Pierre Feuille Ciseaux !")
    joueurA = input ("Joueur A, Pierre Feuille ou Ciseaux ? (P / F / C) ").upper()
    jAnombre = mains[joueurA]
    joueurB = input("Joueur B, Pierre Feuille ou Ciseaux ? (P / F / C) ").upper()
    jBnombre = mains[joueurB]
    if jBnombre - jAnombre == 1 or jBnombre - jAnombre == -2:
        return("JA : {}, JB : {} Le joueur B gagne !".format(joueurA,joueurB))
    elif jBnombre - jAnombre == 0:
        return("JA : {}, JB : {} Égalité !".format(joueurA,joueurB))
    else:
        return("JA : {} ,JB : {} Le joueur A gagne !".format(joueurA,joueurB))
        
        # retourne le chiffre choisi par les 2 joueurs ainsi que le gagnant des deux