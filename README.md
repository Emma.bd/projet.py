# Projet.py


Explication programme


J'ai choisi de faire ce programme car tout le monde connaît bien ce jeu rapide alors je me suis dit que le programmer pouvait être intéressant.
Pour cela, j'ai mélangé les dictionnaires ainsi que des "if", des "return"...
Le but était de créer un programme pas trop difficile et compréhensible, c'est-à-dire, pas surchargé.


Globalement:

Lorsque l'on l'exécute, il demande le choix du premier joueur et du deuxième entre P(Pierre), F(Feuille) ou C(Ciseaux).
Puis en fonction de leurs choix, il renvoie le gagnant ou annonce une égalité.


Plus en détails:

Pour une meilleure compréhension dans le programme, j'ai associe chaque chose à un chiffre (l.5), pour pouvoir faire des calculs plus tard dans le programme.
Ensuite, lorsque le joueur A et le joueur B vont rentrer leurs choix (P,F,C), le programme doit le convertir en un chiffre (1,2,3) comme annoncé dans le dictionnaire.
C'est à cela que servent les lignes 11 et 13, à associer la lettre au chiffe correspondant.

Une fois que l'éditeur sait les choix des 2 joueurs il faut qu'il sache lequel a gagné

Pour cela, j'ai mis un "if". 
J'ai remarqué que lorsque l'on soustrait le chiffre du joueur B avec celui du joueur A et que le résultat était égal à 1 ou à -2, alors le joueur B gagnait.
C'est pour cela que j'ai programmé que si le résultat n'était pas 1 ou -2, le joueur A gagnait.
Et que si le résultat était nul, alors il y avait une égalité.
On voit cela aux lignes 14, 16 et 18